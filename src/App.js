import "./App.css";
import TodoList from "../src/components/TodoList";
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return <TodoList />;
}

export default App;
