import React from "react";
import { Button, Row } from "react-bootstrap";

const Todoitem = ({ todo, cambiarEstado, eliminarTareasCompletadas }) => {
  const { id, task, description, importante, completed } = todo;

  const fnCambiarEstado = () => {
    cambiarEstado(id);
    eliminarTareasCompletadas();
  };
  const colorFondo = () => {
    if (importante) {
      return "#EC7063";
    }
    return "#ffc";
  };
  return (
    <div className="post-it">
      <ul>
        <li>
          <a href="#" style={{ backgroundColor: colorFondo() }}>
            <input
              type="button"
              value="X"
              className="botonCerrar"
              onClick={fnCambiarEstado}
            />
            <h2>{task}</h2>
            <p>{description}</p>
            <p>{importante}</p>
          </a>
        </li>
      </ul>
    </div>
  );
};

export default Todoitem;
