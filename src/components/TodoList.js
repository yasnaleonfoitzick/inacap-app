import React, { Fragment, useState, useRef, useEffect } from "react";
import { v4 as uuid } from "uuid";
import TodoItem from "../components/TodoItem";
import { Button, Row, Col, Form, Card } from "react-bootstrap";

const KEY = "todo";

const TodoList = () => {
  const [todos, setTodos] = useState([]);
  const taskRef = useRef();
  const desRef = useRef();
  const impRef = useRef();

  useEffect(() => {
    const storedTodos = JSON.parse(localStorage.getItem(KEY));
    if (storedTodos) {
      setTodos(storedTodos);
    }
  }, []);

  useEffect(() => {
    localStorage.setItem(KEY, JSON.stringify(todos));
  }, [todos]);

  const agregarTarea = () => {
    console.log("Agregando Tarea");
    const task = taskRef.current.value;
    const des = desRef.current.value;
    const imp = impRef.current.checked;

    if (des === "") return;
    const newTask = {
      id: uuid(),
      task: task,
      description: des,
      importante: imp || false,
      completed: false,
    };

    setTodos([...todos, newTask]);
    taskRef.current.value = null;
    desRef.current.value = null;
    impRef.current.checked = null;
  };

  const ResumenTareas = () => {
    const cant = cantidadTareas();
    if (cant === 0) {
      return (
        <div className="alert alert-success mt-3">
          Felicitaciones no tienes tareas pendientes!
        </div>
      );
    }

    if (cant === 1) {
      return (
        <div className="alert alert-info mt-3">
          Te queda solamente una tarea pendiente!
        </div>
      );
    }

    return (
      <div className="alert alert-info mt-3">
        te quedan {cant} tareas pendientes
      </div>
    );
  };

  const cantidadTareas = () => {
    return todos.filter((todo) => !todo.completed).length;
  };

  const cambiarEstadoTarea = (id) => {
    console.log(id);
    const newTodos = [...todos];
    const todo = newTodos.find((todo) => todo.id === id);
    todo.completed = !todo.completed;
    setTodos(newTodos);
  };

  const eliminarTareasCompletadas = () => {
    const newTodos = todos.filter((todo) => !todo.completed);
    setTodos(newTodos);
  };

  return (
    <Fragment>
      <Card style={{ width: "100%", border: 0, backgroundColor: "#666" }}>
        <Card.Body>
          <Card.Title>Post It Simulator!</Card.Title>
          <Form>
            <Row>
              <Col>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                  <Form.Label>Titulo</Form.Label>
                  <Form.Control
                    type="text"
                    ref={taskRef}
                    placeholder="Ingresa Título de la Tarea"
                  />
                </Form.Group>
              </Col>
              <Col>
                <Form.Group className="mb-3" controlId="formBasicPassword">
                  <Form.Label>Descripción</Form.Label>
                  <Form.Control
                    type="text"
                    ref={desRef}
                    placeholder="Ingresa descripción"
                  />
                </Form.Group>
              </Col>
              <Col className="d-flex align-items-center col-auto">
                <Form.Group className="mt-4" controlId="formBasicCheckbox">
                  <Form.Check
                    type="checkbox"
                    ref={impRef}
                    label="Importante !"
                  />
                </Form.Group>
              </Col>
              <Col className="d-flex align-items-center  ">
                <Button
                  className="mt-4"
                  style={{ backgroundColor: "#111" }}
                  onClick={agregarTarea}
                >
                  Agregar
                </Button>
              </Col>
            </Row>
          </Form>
        </Card.Body>
      </Card>

      <ul className="list-group " style={{ backgroundColor: "#666" }}>
        <Row className="d-flex justify-content-center ">
          {todos.map((todo) => (
            <Col className="d-flex justify-content-center ">
              <TodoItem
                todo={todo}
                key={todo.id}
                cambiarEstado={cambiarEstadoTarea}
                eliminarTareasCompletadas={eliminarTareasCompletadas}
              />
            </Col>
          ))}
        </Row>
      </ul>
      <ResumenTareas />
    </Fragment>
  );
};
export default TodoList;
